"""
Commonly used mocks
Their functionality is consistent from test to test.

Define tailored output on a per-test basis.
"""


from test_semantic_partition_utils.mocks.mock_logging import MockLogger
