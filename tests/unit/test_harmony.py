"""
test_harmony.py

Tests for using harmony to assess semantic partitions
"""

from test_semantic_partition_utils.util import TestUnit

from semantic_partition_utils import harmony


class TestOfDefinitionSet(TestUnit):
    """
    Test getting the harmony of a set of definitions
    """


    def test_empty_set(self):
        """
        Test calculating harmony for an empty set of definitions
        """
        definitions = set()
        def harmony_fnc(definition1, definition2):
            return {}[(definition1, definition2)]
        actual = harmony.of_definition_set(definitions, harmony_fnc)
        expected = 1.0
        self.assertEqual(actual, expected)


    def test_singleton(self):
        """
        Test calculating harmony for a single definition
        """
        definitions = {'d1'}
        def harmony_fnc(definition1, definition2):
            return {}[(definition1, definition2)]
        actual = harmony.of_definition_set(definitions, harmony_fnc)
        expected = 1.0
        self.assertEqual(actual, expected)


    def test_more_definitions(self):
        """
        Test calculating harmony for several definitions
        """
        definitions = {'d1', 'd2', 'd3', 'd4'}
        def harmony_fnc(definition1, definition2):
            return {
                ('d1', 'd2'): 0.9,
                ('d1', 'd3'): 0.8,
                ('d1', 'd4'): 1.0,
                ('d2', 'd3'): 0.6,
                ('d2', 'd4'): 0.8,
                ('d3', 'd4'): 1.0,
            }[(definition1, definition2)]
        actual = harmony.of_definition_set(definitions, harmony_fnc)
        expected = 0.8377137083581048
        self.assertAlmostEqual(actual, expected, 6)


    def test_list_input(self):
        """
        Test calculating harmony providing definitions as a list
        """
        definitions = ['d1', 'd2', 'd3', 'd4']
        def harmony_fnc(definition1, definition2):
            return {
                ('d1', 'd2'): 0.9,
                ('d1', 'd3'): 0.8,
                ('d1', 'd4'): 1.0,
                ('d2', 'd3'): 0.6,
                ('d2', 'd4'): 0.8,
                ('d3', 'd4'): 1.0,
            }[(definition1, definition2)]
        actual = harmony.of_definition_set(definitions, harmony_fnc)
        expected = 0.8377137083581048
        self.assertAlmostEqual(actual, expected, 6)


    def test_zero_harmony(self):
        """
        Test calculating harmony when it should zero out
        """
        definitions = {'d1', 'd2', 'd3', 'd4'}
        def harmony_fnc(definition1, definition2):
            return {
                ('d1', 'd2'): 0.0,
                ('d1', 'd3'): 0.8,
                ('d1', 'd4'): 1.0,
                ('d2', 'd3'): 0.6,
                ('d2', 'd4'): 0.8,
                ('d3', 'd4'): 1.0,
            }[(definition1, definition2)]
        actual = harmony.of_definition_set(definitions, harmony_fnc)
        expected = 0.0
        self.assertAlmostEqual(actual, expected, 6)


class TestOfPartition(TestUnit):
    """
    Test getting the harmony of a set of sets of definitions
    """


    def test_empty_set(self):
        """
        Test calculating harmony for an empty partition
        """
        partition = set()
        def harmony_fnc(definition1, definition2):
            return {}[(definition1, definition2)]
        multiplicity_penalty = 0.95

        actual = harmony.of_partition(
            partition,
            harmony_fnc,
            multiplicity_penalty,
        )
        expected = 1.0
        self.assertEqual(actual, expected)


    def test_normal(self):
        """
        Test calculating harmony for an empty partition
        """
        partition = {
            frozenset({'d1', 'd2'}),
            frozenset({'d3', 'd4', 'd5'}),
            frozenset({'d6', 'd7', 'd8', 'd9'}),
        }
        def harmony_fnc(definition1, definition2):
            return {
                ('d1', 'd2'): 1.0,

                ('d3', 'd4'): 1.0,
                ('d3', 'd5'): 1.0,
                ('d4', 'd5'): 0.9,

                ('d6', 'd7'): 1.0,
                ('d6', 'd8'): 0.8,
                ('d6', 'd9'): 1.0,
                ('d7', 'd8'): 0.9,
                ('d7', 'd9'): 0.9,
                ('d8', 'd9'): 1.0,
            }[(definition1, definition2)]
        multiplicity_penalty = 0.95

        actual = harmony.of_partition(
            partition,
            harmony_fnc,
            multiplicity_penalty,
        )
        expected = 0.8272153696672333
        self.assertAlmostEqual(actual, expected, 6)


    def test_list_input(self):
        """
        Test calculating harmony with lists as input
        """
        partition = [
            ['d1', 'd2'],
            ['d3', 'd4', 'd5'],
        ]
        def harmony_fnc(definition1, definition2):
            return {
                ('d1', 'd2'): 1.0,

                ('d3', 'd4'): 1.0,
                ('d3', 'd5'): 1.0,
                ('d4', 'd5'): 0.9,
            }[(definition1, definition2)]
        multiplicity_penalty = 0.95

        actual = harmony.of_partition(
            partition,
            harmony_fnc,
            multiplicity_penalty,
        )
        expected = 0.8867903574520526
        self.assertAlmostEqual(actual, expected, 6)


class TestFindMaximalPartition(TestUnit):
    """
    Test finding the maximal partition
    """


    def test_no_partitions(self):
        """
        Test finding the maximal partition when there are none
        """
        partitions = []
        def harmony_fnc(definition1, definition2):
            return {}[(definition1, definition2)]
        multiplicity_penalty = 0.95

        actual = harmony.find_maximal_partition(
            partitions,
            harmony_fnc,
            multiplicity_penalty,
        )
        expected = set()
        self.assertEqual(actual, expected)


    def test_small_partitions(self):
        """
        Test finding the maximal partition when there are none
        """
        partitions = [
            frozenset({
                frozenset({'d1', 'd2'}),
                frozenset({'d3', 'd4', 'd5'}),
            }),
            frozenset({
                frozenset({'d1'}),
                frozenset({'d2'}),
                frozenset({'d3', 'd4', 'd5'}),
            }),
        ]
        def harmony_fnc(definition1, definition2):
            return {
                ('d1', 'd2'): 1.0,

                ('d3', 'd4'): 1.0,
                ('d3', 'd5'): 1.0,
                ('d4', 'd5'): 0.9,
            }[(definition1, definition2)]
        multiplicity_penalty = 0.95

        actual = harmony.find_maximal_partition(
            partitions,
            harmony_fnc,
            multiplicity_penalty,
        )
        expected = frozenset({
            frozenset({'d1', 'd2'}),
            frozenset({'d3', 'd4', 'd5'}),
        })
        self.assertEqual(actual, expected)


    def test_early_maximal(self):
        """
        Test finding the maximal partition early
        """
        partitions = [
            frozenset({
                frozenset({'d1', 'd2'}),
            }),
            frozenset({
                frozenset({'d1'}),
                frozenset({'d2'}),
            }),
        ]
        def harmony_fnc(definition1, definition2):
            return {
                ('d1', 'd2'): 1.0,
            }[(definition1, definition2)]
        multiplicity_penalty = 0.95

        actual = harmony.find_maximal_partition(
            partitions,
            harmony_fnc,
            multiplicity_penalty,
        )
        expected = frozenset({
            frozenset({'d1', 'd2'}),
        })
        self.assertEqual(actual, expected)
