"""
test_get_all.py

Tests for getting all partitions of a set
"""

from textwrap import dedent

from test_semantic_partition_utils.util import TestUnit

from semantic_partition_utils import partition


#pylint: disable=duplicate-code
def _comparable_partitions(partitions):
    """
    Make partitions into something comparable
    """
    partitions_c = []
    for part in partitions:
        part_c = []
        for eq_class in part:
            part_c.append(frozenset(eq_class))
        partitions_c.append(frozenset(part_c))
    return frozenset(partitions_c)


class TestGetAll(TestUnit):
    """
    Test getting all partitions
    """


    def _test_case(self, test_case):
        """
        Generic test case handling equality comparisons
        """
        actual = partition.get_all(*test_case["args"])
        actual_c = _comparable_partitions(actual)
        expected_c = _comparable_partitions(test_case["expected"])
        errmsg = dedent(f"""
            partition.get_all() yields {list(partition.get_all(*test_case["args"]))}
            with args {test_case["args"]},
            but should yield {test_case["expected"]}.
            Evaluated:
                actual = {actual_c}
                expected = {expected_c}
        """)
        self.assertSetEqual(actual_c, expected_c, errmsg)


    def test_empty_without_emptyset(self):
        """
        Test getting partitions of an empty set, not including the empty set
        """
        self._test_case({
            "args": (set(), None, False),
            "expected": [],
        })


    def test_empty_including_emptyset(self):
        """
        Test getting partitions of an empty set, including the empty set
        """
        self._test_case({
            "args": (set(),),
            "expected": [[[]]],
        })


    def test_singleton_without_emptyset(self):
        """
        Test getting partitions of a singleton, without the empty set
        """
        self._test_case({
            "args": ({1}, None, False),
            "expected": [ [[1]] ],
        })


    def test_singleton_including_emptyset(self):
        """
        Test getting partitions of a singleton, including the empty set
        """
        self._test_case({
            "args": ({1},),
            "expected": [
                [[1]],
                [[1], []],
            ],
        })


    def test_len3_without_emptyset(self):
        """
        Test getting partitions of a 3-length set, without the empty set
        """
        self._test_case({
            "args": ({1, 2, 3}, None, False),
            "expected": [
                [[1], [2], [3]],
                [[1, 2], [3]],
                [[1], [2, 3]],
                [[1, 3], [2]],
                [[1, 2, 3]],
            ],
        })


    def test_len3_including_emptyset(self):
        """
        Test getting partitions of a 3-length set, including the empty set
        """
        self._test_case({
            "args": ({1, 2, 3},),
            "expected": [
                [[1], [2], [3]],
                [[1, 2], [3]],
                [[1], [2, 3]],
                [[1, 3], [2]],
                [[1, 2, 3]],

                [[1], [2], [3], []],
                [[1, 2], [3], []],
                [[1], [2, 3], []],
                [[1, 3], [2], []],
                [[1, 2, 3], []],
            ],
        })


    def test_criterion(self):
        """
        Test getting partitions with a criterion
        """
        def criterion(elem, elem_set):
            """Determines if elem can be added to elem_set"""
            return all((
                abs(elem - e) < 2
                for e in elem_set
            ))
        self._test_case({
            "args": ({1, 2, 3}, criterion, False),
            "expected": [
                [[1], [2], [3]],
                [[1, 2], [3]],
                [[1], [2, 3]],
            ],
        })
