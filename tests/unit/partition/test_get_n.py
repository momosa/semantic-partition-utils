"""
test_get_n.py

Tests for getting n partitions of a set
"""

from textwrap import dedent

from test_semantic_partition_utils.util import TestUnit

from semantic_partition_utils import partition


#pylint: disable=duplicate-code
def _comparable_partitions(partitions):
    """
    Make partitions into something comparable
    """
    partitions_c = []
    for part in partitions:
        part_c = []
        for eq_class in part:
            part_c.append(frozenset(eq_class))
        partitions_c.append(frozenset(part_c))
    return frozenset(partitions_c)


class TestGetN(TestUnit):
    """
    Test getting n partitions
    """


    def _test_case(self, test_case):
        """
        Generic test case handling equality comparisons
        """
        actual = partition.get_n(**test_case["kwargs"])
        actual_c = _comparable_partitions(actual)
        expected_c = _comparable_partitions(test_case["expected"])
        errmsg = dedent(f"""
            partition.get_n() yields {actual}
            with kwargs {test_case["kwargs"]},
            but should yield {test_case["expected"]}.
            Evaluated:
                actual = {actual_c}
                expected = {expected_c}
        """)
        self.assertSetEqual(actual_c, expected_c, errmsg)


    def test_empty(self):
        """
        Test getting partitions of an empty set
        """
        self._test_case({
            "kwargs": {
                "orig_set": set(),
                "n_partitions": 2,
                "criterion": lambda elem, eq_class: True,
                "random_seed": 1,
            },
            "expected": [[]],
        })


    def test_zero_partitions(self):
        """
        Test getting zero partitions
        """
        self._test_case({
            "kwargs": {
                "orig_set": {1, 2, 3},
                "n_partitions": 0,
                "criterion": lambda elem, eq_class: True,
                "random_seed": 1,
            },
            "expected": [],
        })


    def test_typical(self):
        """
        Test a typical case of getting partitions
        """
        self._test_case({
            "kwargs": {
                "orig_set": {1, 2, 3, 4, 5, 6, 7},
                "n_partitions": 5,
                "criterion": lambda elem, eq_class: all((
                    abs(elem - other) < 3
                    for other in eq_class
                )),
                "random_seed": 1,
            },
            # Interestingly, we get only 3 unique results,
            # which is allowed because we don't guarantee uniqueness
            "expected": [
                [ [1, 2, 3], [4, 5, 6], [7] ],
                [ [2, 3, 4], [5, 6, 7], [1] ],
                [ [1, 2, 3], [5, 6, 7], [4] ],
            ],
        })
