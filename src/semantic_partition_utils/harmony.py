"""
harmony.py

Functions for calculating harmony.
Harmony is a property use to optimize the creation of semantic partitions.

of_definition_set(definitions, harmony_fnc)
    Get the harmony of a set of definitions

of_partition(partition, harmony_fnc)
    Get the harmony of a set of partition of definitions

find_maximal_partition(partitions, harmony_fnc)
    Get the partition that maximizes harmony

"""

import copy
import functools


CACHE_SIZE = 10000


def of_definition_set(
    definitions: set,
    harmony_fnc: callable,
) -> float:
    """
    Get the harmony of a set of definitions

    :param set definitions:
        A set of definitions.
    :param callable harmony_fnc:
        A function taking any two distinct definitions and mapping into the unit interval.
        The function must by symmetric, i.e., h(d1, d2) == h(d2, d1).
        The function must be defined for all pairs of distinct definitions,
        although if h(d1, d2) is defined, h(d2, d1) need not be.
    """
    # Eliminate duplicates for symmetry
    uncounted = set(copy.deepcopy(definitions))
    unique_definition_pairs = []
    while uncounted:
        definition = uncounted.pop()
        new_pairs = [ (definition, d2) for d2 in uncounted ]
        unique_definition_pairs = unique_definition_pairs + new_pairs

    get_harmony = _get_caching_harmony_fnc(harmony_fnc)

    little_harmonies = [
        get_harmony(*(definition_pair))
        for definition_pair in unique_definition_pairs
        if definition_pair[0] != definition_pair[1]
    ]
    return _gmean(little_harmonies)


def of_partition(
    partition: set,
    harmony_fnc: callable,
    multiplicity_penalty: float,
) -> float:
    """
    Get the harmony of a partition (set of sets of definitions)

    :param set partition:
        A set of sets of definitions
    :param callable harmony_fnc:
        A function taking two distinct definitions and mapping into the unit interval.
        The function must by symmetric, i.e., h(d1, d2) == h(d2, d1).
        The function must be defined for all pairs of distinct definitions
        within each equivalence class (set of definitions),
        although if h(d1, d2) is defined, h(d2, d1) need not be.
        For example, if partition = { {d1, d2}, {d3, d4, d5} },
        a minimal harmony function would be defined on the following pairs:
        (d1, d2), (d3, d4), (d3, d5), and (d4, d5)
    :param float multiplicity_penalty:
        A number in the unit interval representing the penalty
        for having more equivalence classes in the partition, other things being equal.
        A value of 1.0 represents no penalty.
        The closer the value is to 0, the higher the penalty.
    """
    eq_class_harmonies = [
        of_definition_set(list(eq_class), harmony_fnc)
        for eq_class in partition
    ]
    return multiplicity_penalty**len(list(partition)) * _gmean(eq_class_harmonies)


def find_maximal_partition(
    partitions: iter,
    harmony_fnc: callable,
    multiplicity_penalty: float,
) -> set:
    """
    Get the harmony of a partition (set of sets of definitions)

    :param iter partitions:
        An iterable over sets of sets of definitions: iter<set<set>>
    :param callable harmony_fnc:
        A function taking two distinct definitions and mapping into the unit interval.
        The function must by symmetric, i.e., h(d1, d2) == h(d2, d1).
        The function must be defined for all pairs of distinct definitions
        within each equivalence class (set of definitions),
        although if h(d1, d2) is defined, h(d2, d1) need not be.
        For example, if a partition = { {d1, d2}, {d3, d4, d5} },
        a minimal harmony function would be defined on the following pairs:
        (d1, d2), (d3, d4), (d3, d5), and (d4, d5).
        The harmony function must by likewise defined for all partitions.
    :param float multiplicity_penalty:
        A number in the unit interval representing the penalty
        for having more equivalence classes in the partition, other things being equal.
        A value of 1.0 represents no penalty.
        The closer the value is to 0, the higher the penalty.
    """
    get_harmony = _get_caching_harmony_fnc(harmony_fnc)
    maximal_partition = set()
    maximal_harmony = 0
    for partition in partitions:
        harmony = of_partition(partition, get_harmony, multiplicity_penalty)
        if harmony > maximal_harmony:
            maximal_harmony = harmony
            maximal_partition = partition
            if maximal_harmony == multiplicity_penalty:
                break
    return maximal_partition


def _gmean(numbers):
    """
    Calculate the geometric mean
    """
    prod = 1
    n_numbers = max(len(list(numbers)), 1.0)
    for num in numbers:
        prod = prod * num
    return prod**(1.0/n_numbers)


def _get_caching_harmony_fnc(harmony_fnc: callable) -> callable:
    """
    Get a version of the harmony function that caches and accounts for symmetry

    Caching approach assumes no one is switching up the harmony fnc
    """
    harmony_cache = {}
    #pylint: disable=bare-except
    def caching_harmony_fnc(harmony_cache, definition1, definition2):
        harmony = None
        if (definition1, definition2) in harmony_cache:
            harmony = harmony_cache[(definition1, definition2)]
        elif (definition2, definition1) in harmony_cache:
            harmony = harmony_cache[(definition2, definition1)]
        else:
            try:
                harmony = harmony_fnc(definition1, definition2)
                harmony_cache[(definition1, definition2)] = harmony
            except:
                harmony = harmony_fnc(definition2, definition1)
                harmony_cache[(definition2, definition1)] = harmony
        return harmony
    return functools.partial(caching_harmony_fnc, harmony_cache)
