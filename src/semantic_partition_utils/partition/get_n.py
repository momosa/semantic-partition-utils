"""
get_n.py

get_n(orig_set, n_partitions, criterion, random_seed)
    Get some partitions of the input set
"""


import copy
import random


def get_n(
    orig_set: set,
    n_partitions: int,
    criterion: callable,
    random_seed: int = None,
) -> set:
    """
    Get some partitions of the input set.

    Does not guarantee uniqueness because
    we don't know a priori how many viable partitions there are.

    Recall:
        A partition of a set S is a set of subsets of S
        that are mutually exclusive and exhaustive.
        The elements of the partition are equivalence classes.

        When working with iterators,
        they go away when you use them up.

    Although this is about sets,
    we use lists for partitions and equivalence classes.
    This is just because python doesn't like mutable sets within sets.

    :param set orig_set:
        The set from which to create partitions
    :param int n_partitions:
        The maximum number of partitions to return.
    :param callable criterion:
        A function criterion(elem, set<elems>) -> bool
        determing whether or not an element can be added to the set.
        If criterion is None (the default),
        assumes any combination of elements is allowed.
        Does not presuppose an equivalence relation.
        Example:
            def criterion(elem, elem_set):
                return all((
                    similarity(elem, e) >= 0.7
                    for e in elem_set
                ))
    :param int random_seed:
        A seed to make randomness perdictable.
        If you don't pass one in,
        consider setting random.seed at the beginning of your program.
    :return: 
        An iterator for all partitions.
    :rtype: iterator<list<list>>
    """
    partitions = []
    if random_seed is not None:
        random.seed(random_seed)
    for _ in range(n_partitions):
        ordered_elems = list(copy.deepcopy(orig_set))
        random.shuffle(ordered_elems)
        partition = _get_one(ordered_elems, criterion)
        partitions.append(partition)

    return iter(partitions)


def _get_one(ordered_elems, criterion):
    """
    Get a single partition.
    Greedily attempts to group elems.
    """
    partition = []
    for elem in ordered_elems:
        new_home = next((
            eq_class for eq_class in partition
            if criterion(elem, eq_class)
        ), None)
        if new_home:
            new_home.append(elem)
        else:
            partition.append([elem])
    return partition
