"""
get_all.py

get_all(orig_set, criterion, include_empty):
    Get all partitions of the input set
"""


import itertools
import copy


def get_all(
    orig_set: set,
    criterion: callable = None,
    include_empty: bool = True,
) -> set:
    """
    Get all partitions of the input set

    Recall:
        A partition of a set S is a set of subsets of S
        that are mutually exclusive and exhaustive.
        The elements of the partition are equivalence classes.

        When working with iterators,
        they go away when you use them up.

    Although this is about sets,
    we use lists for partitions and equivalence classes.
    This is just because python doesn't like mutable sets within sets.

    :param set orig_set:
        The set from which to create partitions
    :param callable criterion:
        A function criterion(elem, set<elems>) -> bool
        determing whether or not an element can be added to the set.
        If criterion is None (the default),
        assumes any combination of elements is allowed.
        Does not presuppose an equivalence relation.
        Example:
            def criterion(elem, elem_set):
                return all((
                    similarity(elem, e) >= 0.7
                    for e in elem_set
                ))
    :param bool include_empty:
        Whether or not to include the empty set in partitions
    :return: 
        An iterator for all partitions.
    :rtype: iterator<list<list>>
    """
    partitions = iter([])
    if not criterion:
        #pylint: disable=unnecessary-lambda-assignment
        criterion = lambda elem, eq_class: True

    for elem_idx, elem in enumerate(orig_set):
        if elem_idx == 0:
            partitions = iter([[[elem]]])
            continue
        new_partitions = iter([])
        for partition in partitions:
            for eq_class in partition:
                if criterion(elem, eq_class):
                    eq_class.append(elem)
                    new_partitions = _append_copy(new_partitions, partition)
                    eq_class.pop()
            partition.append([elem])
            new_partitions = _append_copy(new_partitions, partition)
        partitions = new_partitions

    if include_empty:
        # Make a copy because iterators burn up on use
        partitions_copy = copy.deepcopy(partitions)
        partitions = itertools.chain(
            partitions,
            (_append_copy(p, []) for p in partitions_copy),
        ) if orig_set else iter([[[]]])

    return partitions


def _append_copy(iterator, elem):
    """
    Append the element to the iterator.
    Uses a deep copy of the element.
    """
    return itertools.chain(iterator, (copy.deepcopy(elem),))
