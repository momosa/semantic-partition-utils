"""
partition
    Module for generating partitions
"""

from semantic_partition_utils.partition.get_all import get_all
from semantic_partition_utils.partition.get_n import get_n
