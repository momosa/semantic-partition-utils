"""
semantic_partition_utils

Utilities for semantic partitioning
"""
from setuptools import setup, find_packages
setup(
    name="semantic_partition_utils",
    packages=find_packages(),
    description="Utilities for semantic partitioning",
)
