# semantic-partition-utils

## Overview

Some basic utils for creating semantic partitions

## Technical setup

If you're using these utils for another project,
use the virtual environment for that project.
Skip below to installing requirements.
Just be aware of python version compatibility.

If you're developing these utils or otherwise
working just with these utils,
follow the instructions to set up a virtual environment
for this project.

### Virtual environment setup

These instructions utilize `pyenv` to manage the virtual environment.

Start by installing Python and `pyenv`.
See [Pyenv installation instructions](https://github.com/pyenv/pyenv#installation).

Install the correct version of Python:

```shell
pyenv install 3.11.2
```

Create the virtual environment from the project directory:

```shell
pyenv virtualenv 3.11.2 semantic-partition-utils
```

Now you can activate the virtual environment:

```shell
pyenv activate semantic-partition-utils
```

### Install dependencies

With the virtual environment activated,
install the dependencies.

If you're using these utils,
not development them:

```shell
pip install requirements.txt
```

If you're developing these utils,
you'll need to run tests and such:

```shell
pip install requirements-dev.txt
```
