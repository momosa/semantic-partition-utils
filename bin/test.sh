#!/bin/sh -e


usage() {
cat << END_HELP
USAGE

    test.sh [-h|--help|help|unit|lint|integration|all]

EXAMPLES

    # Print this message
    test.sh help

    # Run all tests
    test.sh
    test.sh all

    # Run unit tests
    test.sh unit

    # Run lint tests
    test.sh lint

    # Run unit and lint tests
    test.sh unit lint

END_HELP
}

TESTS="${@}"
INITIAL_DIR=${PWD}

flop() {
    cd "${INITIAL_DIR}" > /dev/null
    exit 1
}

for t in ${TESTS}; do
    if [ ${t} = '-h' ] || [ ${t} = '--help' ] || [ ${t} = 'help' ]; then
        usage
        exit 0
    fi
done

if [ -z "${TESTS}" ] || [ "${TESTS}" = 'all' ]; then
    TESTS='unit lint'
fi

for t in ${TESTS}; do
    if [ ${t} != 'unit' ] && [ ${t} != 'lint' ]; then
        echo "Unrecognized option '${t}'. Try 'test.sh help'." 1>&2
        exit 1
    fi
done

cd "$(dirname ${0})/.." > /dev/null
for t in ${TESTS}; do
    if [ ${t} = 'unit' ]; then
        pytest \
            --cov=src/semantic_partition_utils \
            --cov-report term-missing \
            --cov-report term:skip-covered \
            tests/unit || flop
    elif [ ${t} = 'lint' ]; then
        pylint --rcfile .pylintrc src/semantic_partition_utils || flop
        pylint --rcfile tests/.pylintrc tests || flop
    fi
done
cd "${INITIAL_DIR}" > /dev/null
