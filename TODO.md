# TODO

1. Add logging
1. Limit cache size (harmony.py)
1. Update README on usage
1. Improve caching of harmony fnc in harmony.py,
   accounting for chained use between functions and also
   possible use of multiple harmony functions.
   (Currently only account for the latter, is necessary for correct results.)
   Maybe a `use_cache` param?
1. Maybe: Create a function to select
   an isolated cluster of definitions related to a given definition
   such that the definitions in the cluster are all pairwise related.
1. Create a function to generate a subset of definitions that are related to no other.
   Given a set of definitions and a starter definition,
   return a subset of the original definitions such that
   no definition in the subset relates to any definition outside of the subset.
1. Create a function to construct a partition (for large sets).
   The constructed partition should be a good candidate for being optimal.
